import { Injectable }   from '@angular/core';
import { HttpClient }   from '@angular/common/http';
import { Observable }   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Table } from './table.model';

@Injectable()
export class TableService {
    private serviceUrl = 'https://jsonplaceholder.typicode.com/users';

    constructor(private http: HttpClient) { }

    public getTable(): Observable<Table[]> {
        return this.http.get<Table[]>(this.serviceUrl);
    }
}
