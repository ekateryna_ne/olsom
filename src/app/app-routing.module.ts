import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeroesComponent} from './heroes/heroes.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { TableComponent }   from './table/table.component';

const routes: Routes = [
    // { path: '', redirectTo: '/table', pathMatch: 'full' },
    { path: 'heroes', component: HeroesComponent },
    { path: 'table', component: TableComponent },
    { path: 'dashboard', component: DashboardComponent  },
    { path: 'detail/:id', component: HeroDetailComponent  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
