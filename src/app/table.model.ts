export interface Table {
  name: string;
  email: string;
  phone: string;
  company: {
    name: string;
  };
}
