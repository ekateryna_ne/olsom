import {Component, OnInit} from '@angular/core';
import {MessageService} from '../message.service';
// import {getElementDepthCount} from "@angular/core/src/render3/state";

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

    constructor(public messageService: MessageService) {
    }

    ngOnInit() {
    };
}
