import { Component, OnInit, ViewChild } from '@angular/core';
import { TableService } from '../table.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { DataSource} from '@angular/cdk/collections';
import { Table } from '../table.model';
//import { MatPaginator } from '@angular/material/paginator';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
    // providers: TableService
})

export class TableComponent implements OnInit {
    constructor(private tableService: TableService) { }
    displayedColumns: string[] = ['name', 'email', 'phone', 'company'];
    dataSource = new MatTableDataSource<Table>(TABLE);
  //  dataSource = new TableDataSource(this.tableService);


    @ViewChild(MatPaginator) paginator: MatPaginator;

    ngOnInit() {
        console.log(this.dataSource)
        this.dataSource.paginator = this.paginator;
    }
    // ngAfterViewChecked() {
    //     this.dataSource.paginator = this.paginator;
    // }
}

export class TableDataSource extends DataSource<any> {
    constructor(private tableService: TableService ) {
        super();
    }

    connect(): Observable<Table[]> {
        // console.log(this.tableService);
        return this.tableService.getTable();
    }

    disconnect() {}
}

const TABLE: Table[] = [
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '5555'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '5555'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '6666'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
    {name: '1111', email: '2222', phone: '3333', company: {name: '4444'}},
];
